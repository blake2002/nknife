﻿using System.Web.Mvc;

namespace NKnife.App.Cute.Site.Manager
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
