﻿using System;

namespace Didaku.Engine.Timeaxis.Base.Controls
{
    public class MenuMethods
    {
        public Action<string> GetTicketMethod { get; set; }
        public Action<string> CallMethod { get; set; }
    }
}