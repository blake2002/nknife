namespace NKnife.App.Cute.Base.Exceptions
{
    public class ActiveParamsDataErrorException : TimeaxisException
    {
        public ActiveParamsDataErrorException(string msg)
            : base(msg)
        {
        }
    }
}