﻿
2014年11月16日：
昨天将原Didaku的排队引擎开发成果移入xknife中；
开始回忆当时的开发过程，最重要的是开发进展到什么地步了，的确不好想起来，所以以后开发过程的记录应该再勤快一些；

2012年11月19日：
通过实现了日志列表，将分页式的表格定位到了jqGrid，第一页的查询已经完成。
下一步将开始尝试jqGrid提交分页的参数。