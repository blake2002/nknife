﻿namespace NKnife.Configuring.Interfaces
{
    public interface IOptionPanelPrecondition
    {
        bool Check();
    }
}