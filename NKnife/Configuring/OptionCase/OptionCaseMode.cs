﻿namespace NKnife.Configuring.OptionCase
{
    /// <summary>选项解决方案模式
    /// </summary>
    public enum OptionCaseMode
    {
        /// <summary>
        /// 周模式
        /// </summary>
        Week,
        /// <summary>
        /// 月模式
        /// </summary>
        Month,
        /// <summary>
        /// 指定时间模式
        /// </summary>
        Specify
    }
}