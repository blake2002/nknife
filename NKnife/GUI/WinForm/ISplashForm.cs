namespace NKnife.GUI.WinForm
{
    /// <summary>
    /// interface for Splash Screen
    /// </summary>
    public interface ISplashForm
    {
        void SetStatusInfo(string newStatusInfo);
    }
}
