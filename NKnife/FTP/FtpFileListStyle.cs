namespace NKnife.FTP
{
    public enum FtpFileListStyle
    {
        UnixStyle,
        WindowsStyle,
        Unknown
    }
}