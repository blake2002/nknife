﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过下列属性集控制。

[assembly: AssemblyTitle("NKnife")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("xknfie.net")]
[assembly: AssemblyProduct("NKnife")]
[assembly: AssemblyCopyright("Copyright © xknfie.net 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("FF91ADC1-A814-4D02-B97E-36422DE23646")]

[assembly: InternalsVisibleTo("NKnife.UnitTest")]

[assembly: AssemblyVersion("15.5.2.413")]
[assembly: AssemblyFileVersionAttribute("15.5.2.413")]
