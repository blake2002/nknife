﻿namespace NKnife.Entities
{
    public enum PersonSex
    {
        /// <summary>
        /// 未知
        /// </summary>
        None = -1,

        /// <summary>
        /// 男
        /// </summary>
        Male = 0,

        /// <summary>
        /// 女
        /// </summary>
        Female = 1,
    }
}