﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("NKnife.NSerial")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("xknfie.net")]
[assembly: AssemblyProduct("NKnife.NSerial")]
[assembly: AssemblyCopyright("Copyright © xknfie.net 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("9b915afc-8879-4dd7-8802-197e90fe722e")]

[assembly: AssemblyVersion("15.1.3.413")]
[assembly: AssemblyFileVersion("15.1.3.413")]
