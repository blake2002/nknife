﻿// Version 1.2
// Date: 2014-03-27
// http://sh.codeplex.com
// Dedicated to Public Domain

using System;
using System.Data;
using System.Globalization;

namespace System.Data.SQLite
{
    public enum SqliteColumnType
    {
        Text,
        DateTime,
        Integer,
        Decimal,
        BLOB
    }
}