﻿using NKnife.IoC;
using NKnife.Kits.SocketKnife.Consoles.Common;
using NKnife.Protocol.Generic;
using NKnife.Tunnel;
using NKnife.Tunnel.Base;
using NKnife.Tunnel.Common;
using NKnife.Tunnel.Filters;
using NKnife.Tunnel.Generic;
using SocketKnife;
using SocketKnife.Generic;
using SocketKnife.Generic.Filters;
using SocketKnife.Interfaces;

namespace NKnife.Kits.SocketKnife.Consoles.Demos
{
    class DemoClient
    {
        private bool _IsInitialized = false;
        private readonly ITunnel _Tunnel = DI.Get<ITunnel>();
        private readonly ISocketClient _Client = DI.Get<KnifeLongSocketClient>();
        private readonly StringProtocolFamily _Family = DI.Get<StringProtocolFamily>();

        public StringProtocolFamily GetFamily()
        {
            return _Family;
        }

        public void Initialize(SocketConfig config, SocketCustomSetting customSetting, BaseProtocolHandler<string> handler)
        {
            if (_IsInitialized) return;

            var heartbeatServerFilter = DI.Get<HeartbeatFilter>();
            heartbeatServerFilter.Heartbeat = new Heartbeat("Client","Server");
            heartbeatServerFilter.Heartbeat.Name = "Client";

            heartbeatServerFilter.Interval = 1000 * 2;
            heartbeatServerFilter.EnableStrictMode = true; //严格模式
            heartbeatServerFilter.HeartBeatMode = HeartBeatMode.Active; 

            var codec = DI.Get<StringCodec>();
            if (codec.StringDecoder.GetType() != customSetting.Decoder)
                codec.StringDecoder = (StringDatagramDecoder)DI.Get(customSetting.Decoder);
            if (codec.StringEncoder.GetType() != customSetting.Encoder)
                codec.StringEncoder = (StringDatagramEncoder)DI.Get(customSetting.Encoder);

            StringProtocolFamily protocolFamily = GetProtocolFamily();
            if (protocolFamily.CommandParser.GetType() != customSetting.CommandParser)
                protocolFamily.CommandParser = (StringProtocolCommandParser) DI.Get(customSetting.CommandParser);

            var protocolFilter = DI.Get<SocketProtocolFilter>();
            protocolFilter.Bind(codec, protocolFamily);
            protocolFilter.AddHandlers(handler);

            _Tunnel.AddFilters(DI.Get<LogFilter>());
            if (customSetting.NeedHeartBeat)
                _Tunnel.AddFilters(heartbeatServerFilter);
            _Tunnel.AddFilters(protocolFilter);

            _Client.Config = config;
            _Client.Configure(customSetting.IpAddress, customSetting.Port);
            _Tunnel.BindDataConnector(_Client);
            _IsInitialized = true;
        }

        private StringProtocolFamily GetProtocolFamily()
        {
            _Family.FamilyName = "socket-kit";

            var custom = DI.Get<StringProtocol>("TestCustom");
            custom.Family = _Family.FamilyName;
            custom.Command = "custom";

            return _Family;
        }

        public void Start()
        {
            if (_Client != null)
                _Client.Start();
        }

        public void Stop()
        {
            if (_Client != null)
                _Client.Stop();
        }

    }
}