﻿using NKnife.Protocol.Generic;

namespace NKnife.Kits.SocketKnife.Consoles.Demos.Protocols
{
    class Register : StringProtocol
    {
        public Register() 
            : base("socket-kit", "register")
        {
        }

    }
}
