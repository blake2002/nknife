﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。

[assembly: AssemblyTitle("NKinfe.Socket")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("xknife.net")]
[assembly: AssemblyProduct("NKinfe.Socket")]
[assembly: AssemblyCopyright("Copyright © xknife.net 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("487c65ed-0834-45ff-bc95-ccc8f0d17fe8")]

[assembly: InternalsVisibleTo("NKnife.Socket.UnitTest")]

[assembly: AssemblyVersion("15.1.1.0410")]
[assembly: AssemblyFileVersion("15.1.1.0410")]